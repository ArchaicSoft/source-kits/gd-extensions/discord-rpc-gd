extends Node

var my_app_id: String = "you_app_id_goes_here"

func _ready():
	# callbacks to pass to discord
	var handlers : Dictionary = {
		"ready"         : Callable(self, "_on_ready"),
		"disconnected"  : Callable(self, "_on_disconnect"),
		"errored"       : Callable(self, "_on_errored"),
		"join_game"     : Callable(self, "_on_join_game"),
		"spectate_game" : Callable(self, "_on_spectate_game"),
		"join_request"  : Callable(self, "_on_join_request")
	}
	
	# set some presence details here
	var presence := DiscordPresence.new()
	presence.details = "Testing Godot 4 Extension"
	presence.large_image_key = "large_image"
	presence.large_image_text = "Example Large Text"
	presence.small_image_key = "small_image"
	presence.small_image_text = "Example Large Text"
	
	# init(app_id, handlers, auto_register, optional steam id)
	DiscordRpc.initialize(my_app_id, handlers, 1, "")
	
	# pass the presence to discord
	DiscordRpc.update_presence(presence)

func _on_ready(
	user_id: String,
	username: String,
	discriminator: String,
	avatar: String
): pass

func _on_disconnect(error_code: int, message: String): pass

func _on_errored(error_code: int, message: String): pass

func _on_join_game(join_secret: String): pass

func _on_spectate_game(spectate_secret: String): pass

func _on_join_request(
	user_id: String,
	username: String,
	discriminator: String,
	avatar: String
): pass
