#include "discord_rpc_gd.h"

#include <godot_cpp/core/class_db.hpp>

static DiscordRpc* _global_inst;// = new DiscordRpc();

static const char* c_str(String input) {
	return strdup(input.ascii().get_data());
}

void DiscordRpc::_bind_methods() {
	_global_inst = new DiscordRpc();

    /* signals */

	ADD_SIGNAL(MethodInfo(
		"ready",
		PropertyInfo(Variant::STRING, "user_id"),
		PropertyInfo(Variant::STRING, "username"),
		PropertyInfo(Variant::STRING, "discriminator"),
		PropertyInfo(Variant::STRING, "avatar")
	));

	ADD_SIGNAL(MethodInfo(
		"disconnected",
		PropertyInfo(Variant::INT, "error_code"),
		PropertyInfo(Variant::STRING, "message")
	));

	ADD_SIGNAL(MethodInfo(
		"errored",
		PropertyInfo(Variant::INT, "error_code"),
		PropertyInfo(Variant::STRING, "message")
	));

	ADD_SIGNAL(MethodInfo(
		"join_game",
		PropertyInfo(Variant::STRING, "join_secret")
	));

	ADD_SIGNAL(MethodInfo(
		"spectate_game",
		PropertyInfo(Variant::STRING, "spectate_secret")
	));

	ADD_SIGNAL(MethodInfo(
		"join_request",
		PropertyInfo(Variant::STRING, "user_id"),
		PropertyInfo(Variant::STRING, "username"),
		PropertyInfo(Variant::STRING, "discriminator"),
		PropertyInfo(Variant::STRING, "avatar")
	));
	
    /* methods */

	ClassDB::bind_static_method(
		"DiscordRpc",
		D_METHOD(
			"initialize",
			"application_id",
			"handlers",
			"auto_register",
			"optional_steam_id"
		),
		&DiscordRpc::initialize,
		NULL, NULL,	DEFVAL(1), DEFVAL("")
	);

	ClassDB::bind_static_method(
		"DiscordRpc",
		D_METHOD("shutdown"),
		&DiscordRpc::shutdown
	);

	ClassDB::bind_static_method(
		"DiscordRpc",
		D_METHOD("run_callbacks"),
		&DiscordRpc::run_callbacks
	);

	ClassDB::bind_static_method(
		"DiscordRpc",
		D_METHOD("update_presence", "presence"),
		&DiscordRpc::update_presence
	);
	
	ClassDB::bind_static_method(
		"DiscordRpc",
		D_METHOD("clear_presence"),
		&DiscordRpc::clear_presence
	);

	ClassDB::bind_static_method(
		"DiscordRpc",
		D_METHOD("respond", "user_id", "reply"),
		&DiscordRpc::respond,
		NULL, DEFVAL(0)
	);

	/* enums */

	BIND_ENUM_CONSTANT(NO);
	BIND_ENUM_CONSTANT(YES);
	BIND_ENUM_CONSTANT(IGNORE);
}

static void onReady(const DiscordUser* request) {
	_global_inst->emit_signal(
		"ready",
		request->userId,
		request->username,
		request->discriminator,
		request->avatar
	);
}

static void onDisconnected(int errorCode, const char* message) {
	_global_inst->emit_signal("disconnected", errorCode, message);
}

static void onErrored(int errorCode, const char* message) {
	_global_inst->emit_signal("errored", errorCode, message);
}

static void onJoinGame(const char* joinSecret) {
	_global_inst->emit_signal("join_game", joinSecret);
}

static void onSpectateGame(const char* spectateSecret) {
	_global_inst->emit_signal("spectate_game", spectateSecret);
}

static void onJoinRequest(const DiscordUser* request) {
	_global_inst->emit_signal(
		"join_request",
		request->userId,
		request->username,
		request->discriminator,
		request->avatar
	);
}

void DiscordRpc::clear_handlers(Dictionary handlers) {
	Array arr; size_t size;

	if (handlers.has("ready")) {
		arr = _global_inst->get_signal_connection_list("ready");
		size = arr.size();
		for(size_t i = 0; i < size; i++)
			_global_inst->disconnect("ready", arr[i]);
	}

	if (handlers.has("disconnected")) {
		arr = _global_inst->get_signal_connection_list("disconnected");
		size = arr.size();
		for(size_t i = 0; i < size; i++)
			_global_inst->disconnect("disconnected", arr[i]);
	}

	if (handlers.has("errored")) {
		arr = _global_inst->get_signal_connection_list("errored");
		size = arr.size();
		for(size_t i = 0; i < size; i++)
			_global_inst->disconnect("errored", arr[i]);
	}

	if (handlers.has("join_game")) {
		arr = _global_inst->get_signal_connection_list("join_game");
		size = arr.size();
		for(size_t i = 0; i < size; i++)
			_global_inst->disconnect("join_game", arr[i]);
	}

	if (handlers.has("spectate_game")) {
		arr = _global_inst->get_signal_connection_list("spectate_game");
		size = arr.size();
		for(size_t i = 0; i < size; i++)
			_global_inst->disconnect("spectate_game", arr[i]);
	}
		
	if (handlers.has("join_request")) {
		arr = _global_inst->get_signal_connection_list("join_request");
		size = arr.size();
		for(size_t i = 0; i < size; i++)
			_global_inst->disconnect("join_request", arr[i]);
	}
}

void DiscordRpc::set_handlers(Dictionary handlers) {
	clear_handlers(handlers);

	if (handlers.has("ready"))
		_global_inst->connect("ready", (Callable)handlers["ready"]);

	if (handlers.has("disconnected"))
		_global_inst->connect("disconnected", (Callable)handlers["disconnected"]);

	if (handlers.has("errored"))
		_global_inst->connect("errored", (Callable)handlers["errored"]);

	if (handlers.has("join_game"))
		_global_inst->connect("join_game", (Callable)handlers["join_game"]);

	if (handlers.has("spectate_game"))
		_global_inst->connect("spectate_game", (Callable)handlers["spectate_game"]);
		
	if (handlers.has("join_request"))
		_global_inst->connect("join_request", (Callable)handlers["join_request"]);
}

void DiscordRpc::initialize(
	String application_id,
	Dictionary handlers,
	int auto_register,
	String optional_steam_id
) {
	DiscordEventHandlers evt;
	memset(&evt, 0, sizeof(evt));
	evt.ready = onReady;
	evt.disconnected = onDisconnected;
	evt.errored = onErrored;
	evt.joinGame = onJoinGame;
	evt.spectateGame = onSpectateGame;
	evt.joinRequest = onJoinRequest;

	// DiscordRpc::set_handlers(handlers);

	Discord_Initialize(
		c_str(application_id),
		&evt,
		auto_register,
		optional_steam_id == "" ? NULL : c_str(optional_steam_id)
	);
}

void DiscordRpc::shutdown() {
	Discord_Shutdown();
}

void DiscordRpc::run_callbacks() {
	Discord_RunCallbacks();
}

void DiscordRpc::update_presence(Ref<DiscordPresence> presence) {
	DiscordRichPresence rpc;
	memset(&rpc, 0, sizeof(rpc));
	rpc.state          = c_str(presence->state);
	rpc.details        = c_str(presence->details);
	rpc.startTimestamp = presence->start_timestamp;
	rpc.endTimestamp   = presence->end_timestamp;
	rpc.largeImageKey  = c_str(presence->large_image_key);
	rpc.largeImageText = c_str(presence->large_image_text);
	rpc.smallImageKey  = c_str(presence->small_image_key);
	rpc.smallImageText = c_str(presence->small_image_text);
	rpc.partyId        = c_str(presence->party_id);
	rpc.partySize      = presence->party_size;
	rpc.partyMax       = presence->party_max;
	rpc.matchSecret    = c_str(presence->match_secret);
	rpc.joinSecret     = c_str(presence->join_secret);
	rpc.spectateSecret = c_str(presence->spectate_secret);

	Discord_UpdatePresence(&rpc);
}

void DiscordRpc::clear_presence() {
	Discord_ClearPresence();
}

void DiscordRpc::respond(String user_id, /* DISCORD_REPLY_ */ int reply) {
	Discord_Respond(
		c_str(user_id),
		reply < 1 || reply > 2 ? 0 : reply
	);
}
