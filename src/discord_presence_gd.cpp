#include "discord_presence_gd.h"

#include <godot_cpp/core/class_db.hpp>

void DiscordPresence::_bind_methods() {
    /* state */
	ClassDB::bind_method(D_METHOD("get_state"), &DiscordPresence::get_state);
	ClassDB::bind_method(D_METHOD("set_state", "value"), &DiscordPresence::set_state);
	ADD_PROPERTY(PropertyInfo(Variant::STRING, "state"), "set_state", "get_state");
    
    /* details */
	ClassDB::bind_method(D_METHOD("get_details"), &DiscordPresence::get_details);
	ClassDB::bind_method(D_METHOD("set_details", "value"), &DiscordPresence::set_details);
	ADD_PROPERTY(PropertyInfo(Variant::STRING, "details"), "set_details", "get_details");
    
    /* start_timestamp */
	ClassDB::bind_method(D_METHOD("get_start_timestamp"), &DiscordPresence::get_start_timestamp);
	ClassDB::bind_method(D_METHOD("set_start_timestamp", "value"), &DiscordPresence::set_start_timestamp);
	ADD_PROPERTY(PropertyInfo(Variant::INT, "start_timestamp"), "set_start_timestamp", "get_start_timestamp");
    
    /* end_timestamp */
	ClassDB::bind_method(D_METHOD("get_end_timestamp"), &DiscordPresence::get_end_timestamp);
	ClassDB::bind_method(D_METHOD("set_end_timestamp", "value"), &DiscordPresence::set_end_timestamp);
	ADD_PROPERTY(PropertyInfo(Variant::INT, "end_timestamp"), "set_end_timestamp", "get_end_timestamp");
    
    /* large_image_key */
	ClassDB::bind_method(D_METHOD("get_large_image_key"), &DiscordPresence::get_large_image_key);
	ClassDB::bind_method(D_METHOD("set_large_image_key", "value"), &DiscordPresence::set_large_image_key);
	ADD_PROPERTY(PropertyInfo(Variant::STRING, "large_image_key"), "set_large_image_key", "get_large_image_key");
    
    /* large_image_text */
	ClassDB::bind_method(D_METHOD("get_large_image_text"), &DiscordPresence::get_large_image_text);
	ClassDB::bind_method(D_METHOD("set_large_image_text", "value"), &DiscordPresence::set_large_image_text);
	ADD_PROPERTY(PropertyInfo(Variant::STRING, "large_image_text"), "set_large_image_text", "get_large_image_text");
    
    /* small_image_key */
	ClassDB::bind_method(D_METHOD("get_small_image_key"), &DiscordPresence::get_small_image_key);
	ClassDB::bind_method(D_METHOD("set_small_image_key", "value"), &DiscordPresence::set_small_image_key);
	ADD_PROPERTY(PropertyInfo(Variant::STRING, "small_image_key"), "set_small_image_key", "get_small_image_key");
    
    /* small_image_text */
	ClassDB::bind_method(D_METHOD("get_small_image_text"), &DiscordPresence::get_small_image_text);
	ClassDB::bind_method(D_METHOD("set_small_image_text", "value"), &DiscordPresence::set_small_image_text);
	ADD_PROPERTY(PropertyInfo(Variant::STRING, "small_image_text"), "set_small_image_text", "get_small_image_text");
    
    /* party_id */
	ClassDB::bind_method(D_METHOD("get_party_id"), &DiscordPresence::get_party_id);
	ClassDB::bind_method(D_METHOD("set_party_id", "value"), &DiscordPresence::set_party_id);
	ADD_PROPERTY(PropertyInfo(Variant::STRING, "party_id"), "set_party_id", "get_party_id");
    
    /* party_size */
	ClassDB::bind_method(D_METHOD("get_party_size"), &DiscordPresence::get_party_size);
	ClassDB::bind_method(D_METHOD("set_party_size", "value"), &DiscordPresence::set_party_size);
	ADD_PROPERTY(PropertyInfo(Variant::INT, "party_size"), "set_party_size", "get_party_size");
    
    /* party_max */
	ClassDB::bind_method(D_METHOD("get_party_max"), &DiscordPresence::get_party_max);
	ClassDB::bind_method(D_METHOD("set_party_max", "value"), &DiscordPresence::set_party_max);
	ADD_PROPERTY(PropertyInfo(Variant::INT, "party_max"), "set_party_max", "get_party_max");
    
    /* match_secret */
	ClassDB::bind_method(D_METHOD("get_match_secret"), &DiscordPresence::get_match_secret);
	ClassDB::bind_method(D_METHOD("set_match_secret", "value"), &DiscordPresence::set_match_secret);
	ADD_PROPERTY(PropertyInfo(Variant::STRING, "match_secret"), "set_match_secret", "get_match_secret");
    
    /* join_secret */
	ClassDB::bind_method(D_METHOD("get_join_secret"), &DiscordPresence::get_join_secret);
	ClassDB::bind_method(D_METHOD("set_join_secret", "value"), &DiscordPresence::set_join_secret);
	ADD_PROPERTY(PropertyInfo(Variant::STRING, "join_secret"), "set_join_secret", "get_join_secret");
    
    /* spectate_secret */
	ClassDB::bind_method(D_METHOD("get_spectate_secret"), &DiscordPresence::get_spectate_secret);
	ClassDB::bind_method(D_METHOD("set_spectate_secret", "value"), &DiscordPresence::set_spectate_secret);
	ADD_PROPERTY(PropertyInfo(Variant::STRING, "spectate_secret"), "set_spectate_secret", "get_spectate_secret");
}

String    DiscordPresence::get_state() { return state; }
String    DiscordPresence::get_details() { return details; }
int       DiscordPresence::get_start_timestamp() { return start_timestamp; }
int       DiscordPresence::get_end_timestamp() { return end_timestamp; }
String    DiscordPresence::get_large_image_key() { return large_image_key; }
String    DiscordPresence::get_large_image_text() { return large_image_text; }
String    DiscordPresence::get_small_image_key() { return small_image_key; }
String    DiscordPresence::get_small_image_text() { return small_image_text; }
String    DiscordPresence::get_party_id() { return party_id; }
int       DiscordPresence::get_party_size() { return party_size; }
int       DiscordPresence::get_party_max() { return party_max; }
String    DiscordPresence::get_match_secret() { return match_secret; }
String    DiscordPresence::get_join_secret() { return join_secret; }
String    DiscordPresence::get_spectate_secret() { return spectate_secret; }
    
void DiscordPresence::set_state(String value) { state = value; }
void DiscordPresence::set_details(String value) { details = value; }
void DiscordPresence::set_start_timestamp(int value) { start_timestamp = value; }
void DiscordPresence::set_end_timestamp(int value) { end_timestamp = value; }
void DiscordPresence::set_large_image_key(String value) { large_image_key = value; }
void DiscordPresence::set_large_image_text(String value) { large_image_text = value; }
void DiscordPresence::set_small_image_key(String value) { small_image_key = value; }
void DiscordPresence::set_small_image_text(String value) { small_image_text = value; }
void DiscordPresence::set_party_id(String value) { party_id = value; }
void DiscordPresence::set_party_size(int value) { party_size = value; }
void DiscordPresence::set_party_max(int value) { party_max = value; }
void DiscordPresence::set_match_secret(String value) { match_secret = value; }
void DiscordPresence::set_join_secret(String value) { join_secret = value; }
void DiscordPresence::set_spectate_secret(String value) { spectate_secret = value; }