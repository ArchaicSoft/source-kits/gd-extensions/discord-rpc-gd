#ifndef DISCORD_RPC_GD_H
#define DISCORD_RPC_GD_H

#include "discord_presence_gd.h"

#include "discord_rpc.h"

#include <godot_cpp/classes/ref_counted.hpp>
#include <godot_cpp/core/binder_common.hpp>

using namespace godot;

class DiscordRpc : public RefCounted {
	GDCLASS(DiscordRpc, RefCounted);

protected:
	static void _bind_methods();

public:
    enum DiscordReply {	NO, YES, IGNORE	};

    static void clear_handlers(Dictionary handlers);
    static void set_handlers(Dictionary handlers);

	static void initialize(
        String application_id,
	    Dictionary handlers,
        int auto_register,
        String optional_steam_id
    );
    static void shutdown();
    static void run_callbacks();
    static void update_presence(Ref<DiscordPresence> presence);
    static void clear_presence();
    static void respond(String user_id, /* DISCORD_REPLY_ */ int reply);
};

VARIANT_ENUM_CAST(DiscordRpc::DiscordReply);

#endif // DISCORD_RPC_GD_H
