#ifndef DISCORD_RPC_GD_REGISTER_TYPES_H
#define DISCORD_RPC_GD_REGISTER_TYPES_H

#include <godot_cpp/core/class_db.hpp>

using namespace godot;

void initialize_discord_rpc_module(ModuleInitializationLevel p_level);
void uninitialize_discord_rpc_module(ModuleInitializationLevel p_level);

#endif // DISCORD_RPC_GD_REGISTER_TYPES_H
