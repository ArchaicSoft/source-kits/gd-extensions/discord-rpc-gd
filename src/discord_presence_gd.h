
#ifndef DISCORD_PRESENCE_GD_H
#define DISCORD_PRESENCE_GD_H

#include <godot_cpp/classes/ref_counted.hpp>
#include <godot_cpp/core/binder_common.hpp>

using namespace godot;

class DiscordPresence : public RefCounted {
	GDCLASS(DiscordPresence, RefCounted);

protected:
	static void _bind_methods();

public:
    String    state;
    String    details;
    int       start_timestamp;
    int       end_timestamp;
    String    large_image_key;
    String    large_image_text;
    String    small_image_key;
    String    small_image_text;
    String    party_id;
    int       party_size;
    int       party_max;
    String    match_secret;
    String    join_secret;
    String    spectate_secret;

    String    get_state();
    String    get_details();
    int       get_start_timestamp();
    int       get_end_timestamp();
    String    get_large_image_key();
    String    get_large_image_text();
    String    get_small_image_key();
    String    get_small_image_text();
    String    get_party_id();
    int       get_party_size();
    int       get_party_max();
    String    get_match_secret();
    String    get_join_secret();
    String    get_spectate_secret();
    
    void      set_state(String value);
    void      set_details(String value);
    void      set_start_timestamp(int value);
    void      set_end_timestamp(int value);
    void      set_large_image_key(String value);
    void      set_large_image_text(String value);
    void      set_small_image_key(String value);
    void      set_small_image_text(String value);
    void      set_party_id(String value);
    void      set_party_size(int value);
    void      set_party_max(int value);
    void      set_match_secret(String value);
    void      set_join_secret(String value);
    void      set_spectate_secret(String value);
};

#endif // DISCORD_PRESENCE_GD_H