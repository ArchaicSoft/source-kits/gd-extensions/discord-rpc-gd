# Godot 4 - Discord RPC

This is a Godot 4 based Discord RPC extension.

API is following gdscript standards, works as similar to the official documentation as possible.

This should work properly cross platform, though is only tested on windows.

# Building yourself

On all platforms you will need the latest python 3.x install available, which also requires scons (prefered install with pip, which is another install, or optional you can go for the portable setup on scons website)

On windows you will need VS2022 with C++ desktop development (potentially works with backwards towards 2017, but not recommended if it does)

On linux you will need gcc and g++ (most likely preinstalled, most distros include development tools)

On macos, Xcode will probably work, but if not, install gcc with brew.

simply cd to this directory (where the readme is located) in a terminal, and invoke scons (if using portable you need to specify the full path to the portable binary) with one of the following commands:

scons target=template_debug

scons target=template_release

The above commands simply build the C++ GD Extension which will be found in:

./gdproj/addons/discord_rpc

If testing within godot from editor debug, you will need a template_debug binary for the current platform you are using, you will need the release version for export in release mode only.

# Using in a project

Simply begin by cloning the ./gdproj/addons folder to your godot project, and copy the code in main.gd (modify to your prefered code style and use case).

No plugin settings need enabled, and you can even activate discord rpc from the editor itself by using @tool on a script and using properties to button enable it, or the _ready() func. It is not recommended to update within _process() functions inside the editor without conditions to spread it out.

For discord-rpc updates inside _ready() to work within the editor, you either need to attach the script to a node and open the scene fresh (after a save of course), or add class_name to the top of the script and restart the editor and it will always invoke by opening the project in the editor.

# Support
Bug reports should be reported in the issues here, however they may also be reported on my community discord -- along with questions and feature suggestions.

ArchaicSoft Community Discord:
https://discord.gg/JGSRvak